package com.example.geekbull;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import jakarta.annotation.PostConstruct;

@SpringBootApplication
public class GeekbullApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeekbullApplication.class, args);
	}
	@PostConstruct
    public void afterInit() {
        System.out.println("Application initialized");
    }

    private static void createLogDirectoryIfNeeded() {
        String localLogDirPath = "logs";
        createDirectory(localLogDirPath);
    }

    private static void createDirectory(String directoryPath) {
        File directory = new File(directoryPath);
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                System.err.println("Failed to create directory: " + directoryPath);
            }
        }
    }

}
