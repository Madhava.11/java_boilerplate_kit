package com.example.geekbull.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HelloGeekController {
	
	@GetMapping("/geekbull")
	public String geekbull() {
		return "Hello mate, We are from GeekBull Consulting";
	}

}
